const fs = require("fs");

// to read files
function readData(filename, callback) {
  fs.readFile(filename, "utf-8", (err, data) => {
    if (err) {
      callback(Error("reading data.json failed"), null);
    } else {
      callback(null, data);
    }
  });
}

// to create a file
function writeFile(filename, data, callback) {
  fs.writeFile(filename, data, "utf-8", (err) => {
    if (err) {
      // console.error(err);
      callback(Error(`could not create file ${filename}`), null);
    } else {
      callback(null, `file ${filename} created`);
    }
  });
}

// Retrieve data for ids : [2, 13, 23].
function getIDsData(data, callback) {
  if (typeof data != "object") {
    callback(Error("data is empty or data is not readable"), null);
  } else {
    let Idsdata = data["employees"].reduce((accumulator, employee) => {
      if ([2, 13, 23].includes(employee.id)) {
        accumulator.push(employee);
      }
      return accumulator;
    }, []);
    callback(null, Idsdata);
  }
}

// Group data based on companies.
function groupByCompanies(data, callback) {
  if (typeof data != "object") {
    callback(Error("data is empty or data is not readable"), null);
  } else {
    let companyGroups = data["employees"].reduce((accumulator, employee) => {
      if (accumulator[employee.company]) {
        accumulator[employee.company].push(employee);
      } else {
        accumulator[employee.company] = [employee];
      }
      return accumulator;
    }, {});
    callback(null, companyGroups);
  }
}

// Get all data for company Powerpuff Brigade
function powerpuffBrigade(data, callback) {
  if (typeof data != "object") {
    callback(Error("data is empty or data is not readable"), null);
  } else {
    let powerpuffData = data["employees"].reduce((accumulator, employee) => {
      if (employee.company == "Powerpuff Brigade") {
        accumulator.push(employee);
      }
      return accumulator;
    }, []);
    callback(null, powerpuffData);
  }
}

// Remove entry with id 2.
function deleteID2(data, callback) {
  if (typeof data != "object") {
    callback(Error("data is empty or data is not readable", null));
  } else {
    let dataWithout2 = [];
    data["employees"].forEach((employee) => {
      if (employee.id != 2) {
        dataWithout2.push(employee);
      }
    });
    let dataWithout2Object = {};
    dataWithout2Object["employees"] = dataWithout2;
    callback(null, dataWithout2Object);
  }
}

// Sort data based on company name. If the company name is same, use id as the secondary sort metric.
function sortData(data, callback) {
  if (typeof data != "object") {
    callback(Error("data is empty or data is not readable", null));
  } else {
    let employees = data["employees"];
    employees.sort((employee1, employee2) =>
      employee1.company > employee2.company
        ? 1
        : employee1.company === employee2.company
        ? employee1.id > employee2.id
          ? 1
          : -1
        : -1
    );
    let sortDataObject = {};
    sortDataObject["employees"] = employees;
    callback(null, sortDataObject);
  }
}

// Swap position of companies with id 93 and id 92.
function swap92_93(data, callback) {
  if (typeof data != "object") {
    callback(Error("data is empty or data is not readable"));
  } else {
    ids = data["employees"].reduce((accumulator, employee) => {
      accumulator.push(employee.id);
      return accumulator;
    }, []);
    let employeeList = data["employees"];
    if (ids.includes(92) && ids.includes(93)) {
      let employee92 = data.employees[ids.indexOf(92)];
      let employee93 = data.employees[ids.indexOf(93)];
      employeeList[ids.indexOf(93)] = employee92;
      employeeList[ids.indexOf(92)] = employee93;
      data["employees"] = employeeList;
      callback(null, data);
    } else {
      callback(Error("no data available for ids 92 or 93 or both"));
    }
  }
}

// For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.
function evenIdBirthdays(data, callback) {
  if (typeof data != "object") {
    callback(Error("data is empty or data is not readable"));
  } else {
    let evenBirthdays = data["employees"].reduce((accumulator, employee) => {
      if (employee.id % 2 == 0) {
        const currentDate = new Date();
        employee.age = currentDate;
        accumulator.push(employee);
      } else {
        accumulator.push(employee);
      }
      return accumulator;
    }, []);
    // console.log(evenBirthdays);
    data["employees"] = evenBirthdays;
    callback(null, data);
  }
}

function main() {
  readData("./data.json", (err, data) => {
    if (err) {
      console.error(err.name, err.message);
    } else {
      data = JSON.parse(data);
      getIDsData(data, (err, Idsdata) => {
        if (err) {
          console.error(err.name, err.message);
        } else {
          writeFile(
            "./IDsdata.json",
            JSON.stringify(Idsdata),
            (err, message) => {
              if (err) {
                console.error(err.name, err.message);
              } else {
                console.log(message);
                groupByCompanies(data, (err, companyGroups) => {
                  if (err) {
                    console.error(err.name, err.message);
                  } else {
                    writeFile(
                      "./employeeGroups.json",
                      JSON.stringify(companyGroups),
                      (err, message) => {
                        if (err) {
                          console.error(err.name, err.message);
                        } else {
                          console.log(message);
                          powerpuffBrigade(data, (err, powerpuffData) => {
                            if (err) {
                              console.error(err);
                            } else {
                              writeFile(
                                "./powerPuffData.json",
                                JSON.stringify(powerpuffData),
                                (err, message) => {
                                  if (err) {
                                    console.error(err.name, err.messaage);
                                  } else {
                                    console.log(message);
                                    deleteID2(data, (err, dataWithout2) => {
                                      if (err) {
                                        console.error(err);
                                      } else {
                                        writeFile(
                                          "./dataWithoutId2.json",
                                          JSON.stringify(dataWithout2),
                                          (err, message) => {
                                            if (err) {
                                              console.error(err);
                                            } else {
                                              console.log(message);
                                              sortData(
                                                dataWithout2,
                                                (err, sortData) => {
                                                  if (err) {
                                                    console.err(err);
                                                  } else {
                                                    writeFile(
                                                      "./sortData.json",
                                                      JSON.stringify(sortData),
                                                      (err, message) => {
                                                        if (err) {
                                                          console.error(err);
                                                        } else {
                                                          console.log(message);
                                                          swap92_93(
                                                            sortData,
                                                            (
                                                              err,
                                                              swappedData
                                                            ) => {
                                                              if (err) {
                                                                console.error(
                                                                  err
                                                                );
                                                              } else {
                                                                writeFile(
                                                                  "./swapped92_93.json",
                                                                  JSON.stringify(
                                                                    sortData
                                                                  ),
                                                                  (
                                                                    err,
                                                                    message
                                                                  ) => {
                                                                    if (err) {
                                                                      console.error(
                                                                        err
                                                                      );
                                                                    } else {
                                                                      console.log(
                                                                        message
                                                                      );
                                                                      evenIdBirthdays(
                                                                        sortData,
                                                                        (
                                                                          err,
                                                                          evenBirthdays
                                                                        ) => {
                                                                          if (
                                                                            err
                                                                          ) {
                                                                            console.log(
                                                                              err
                                                                            );
                                                                          } else {
                                                                            writeFile(
                                                                              "./evenIdBirthdays.json",
                                                                              JSON.stringify(
                                                                                evenBirthdays
                                                                              ),
                                                                              (
                                                                                err,
                                                                                message
                                                                              ) => {
                                                                                if (
                                                                                  err
                                                                                ) {
                                                                                  console.error(
                                                                                    err
                                                                                  );
                                                                                } else {
                                                                                  console.log(
                                                                                    message
                                                                                  );
                                                                                }
                                                                              }
                                                                            );
                                                                          }
                                                                        }
                                                                      );
                                                                    }
                                                                  }
                                                                );
                                                              }
                                                            }
                                                          );
                                                        }
                                                      }
                                                    );
                                                  }
                                                }
                                              );
                                            }
                                          }
                                        );
                                      }
                                    });
                                  }
                                }
                              );
                            }
                          });
                        }
                      }
                    );
                  }
                });
              }
            }
          );
        }
      });
    }
  });
}
main();
